## Getting Started

Download, unzip the folder and install the dependencies such as the npm lib or the dotnet core lib 


## The folder consists of

* kwms-electron-node-api-master.zip - node api with mongodb 
* kwms-electron-vue-master.zip - Electron with Vue 
* ng7-demo-master.zip - Angular 7 demo
* RFScan_Vue-master.zip - RFScan - Vue
* RFScan2-master.zip - RFScan Android
* sass-demo-master.zip  - SASS (Css) demo